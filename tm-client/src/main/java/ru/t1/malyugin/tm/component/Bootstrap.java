package ru.t1.malyugin.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.malyugin.tm.api.endpoint.*;
import ru.t1.malyugin.tm.api.repository.ICommandRepository;
import ru.t1.malyugin.tm.api.service.*;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.malyugin.tm.exception.system.CommandNotSupportedException;
import ru.t1.malyugin.tm.repository.CommandRepository;
import ru.t1.malyugin.tm.service.CommandService;
import ru.t1.malyugin.tm.service.LoggerService;
import ru.t1.malyugin.tm.service.PropertyService;
import ru.t1.malyugin.tm.service.TokenService;
import ru.t1.malyugin.tm.util.SystemUtil;
import ru.t1.malyugin.tm.util.TerminalUtil;

import javax.xml.ws.soap.SOAPFaultException;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator, IEndpointLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.malyugin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registryCommandClass(clazz);
    }

    @SneakyThrows
    private void registryCommandClass(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registryCommand(command);
    }

    private void registryCommand(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        command.setEndpointLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TM CLIENT **");
    }

    private void initLog4j() {
        BasicConfigurator.configure();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.print("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]\n");
                loggerService.command(command);
            } catch (@NotNull final SOAPFaultException soapFaultException) {
                loggerService.errorSOAP(soapFaultException);
                System.out.println("[FAIL]\n");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]\n");
            }
        }
    }

    public void processCommand(@Nullable final String command) {
        if (StringUtils.isBlank(command)) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        processArgument(args[0]);
        return true;
    }

    private void processArgument(@Nullable final String argument) {
        if (StringUtils.isBlank(argument)) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void prepareStartup() {
        initPID();
        initLogger();
        initLog4j();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TM CLIENT IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void run(@Nullable final String... args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        processCommands();
    }

}