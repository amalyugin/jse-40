package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.dto.request.user.*;
import ru.t1.malyugin.tm.marker.SoapCategory;
import ru.t1.malyugin.tm.model.User;

import static ru.t1.malyugin.tm.TestData.*;

@Category(SoapCategory.class)
public final class UserEndpointTest {

    @NotNull
    private static final String TEST_SOAP_LOGIN = PROPERTY_SERVICE.getTestSoapLogin();

    @NotNull
    private static final String TEST_SOAP_PASS = PROPERTY_SERVICE.getTestSoapPass();

    @NotNull
    private static final String TEST_ADMIN_LOGIN = PROPERTY_SERVICE.getTestAdminLogin();

    @NotNull
    private static final String TEST_ADMIN_PASS = PROPERTY_SERVICE.getTestAdminPassword();

    @BeforeClass
    public static void setToken() {
        @NotNull final UserLoginRequest loginRequestUsual = new UserLoginRequest(TEST_SOAP_LOGIN, TEST_SOAP_PASS);
        @Nullable final String usualToken = AUTH_ENDPOINT.login(loginRequestUsual).getToken();
        Assert.assertNotNull(usualToken);
        TOKEN_SERVICE_USUAL.setToken(usualToken);

        @NotNull final UserLoginRequest loginRequestAdmin = new UserLoginRequest(TEST_ADMIN_LOGIN, TEST_ADMIN_PASS);
        @Nullable final String adminToken = AUTH_ENDPOINT.login(loginRequestAdmin).getToken();
        Assert.assertNotNull(adminToken);
        TOKEN_SERVICE_ADMIN.setToken(adminToken);
    }

    @AfterClass
    public static void clearToken() {
        @NotNull final UserLogoutRequest requestUsual = new UserLogoutRequest(TOKEN_SERVICE_USUAL.getToken());
        AUTH_ENDPOINT.logout(requestUsual);

        @NotNull final UserLogoutRequest requestAdmin = new UserLogoutRequest(TOKEN_SERVICE_ADMIN.getToken());
        AUTH_ENDPOINT.logout(requestAdmin);
    }

    @NotNull
    private static String getUsualToken() {
        return TOKEN_SERVICE_USUAL.getToken();
    }

    @NotNull
    private static String getAdminToken() {
        return TOKEN_SERVICE_ADMIN.getToken();
    }

    @Test
    public void testUserLockUnlock() {
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(getAdminToken(), TEST_SOAP_LOGIN);
        Assert.assertNotNull(USER_ENDPOINT.lockUser(lockRequest));
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest(TEST_SOAP_LOGIN, TEST_SOAP_PASS);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.login(requestLogin));
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(getAdminToken(), TEST_SOAP_LOGIN);
        Assert.assertNotNull(USER_ENDPOINT.unlockUser(unlockRequest).getUser());
        Assert.assertNotNull(AUTH_ENDPOINT.login(requestLogin).getToken());
    }

    @Test
    public void testChangePassword() {
        @NotNull final String newPass = "NEW_PASS";
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(getUsualToken(), newPass);
        Assert.assertNotNull(USER_ENDPOINT.changePassword(changePasswordRequest).getUser());
        @NotNull final UserLoginRequest loginRequestError = new UserLoginRequest(TEST_SOAP_LOGIN, TEST_SOAP_PASS);
        @NotNull final UserLoginRequest loginRequestSuccess = new UserLoginRequest(TEST_SOAP_LOGIN, newPass);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.login(loginRequestError));
        Assert.assertNotNull(AUTH_ENDPOINT.login(loginRequestSuccess).getToken());
        @NotNull final UserChangePasswordRequest changePasswordRequestBack = new UserChangePasswordRequest(getUsualToken(), TEST_SOAP_PASS);
        Assert.assertNotNull(USER_ENDPOINT.changePassword(changePasswordRequestBack).getUser());
    }

    @Test
    public void testLogin() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(TEST_SOAP_LOGIN, TEST_SOAP_PASS);
        Assert.assertNotNull(AUTH_ENDPOINT.login(loginRequest).getToken());

        @NotNull final String wrongPass = "WRONG";
        @NotNull final UserLoginRequest loginRequestError = new UserLoginRequest(TEST_SOAP_LOGIN, wrongPass);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.login(loginRequestError));
    }

    @Test
    public void testRegistry() {
        @NotNull final String newUsualLogin = "tst_usr";
        @NotNull final String newUsualPass = "tst_pass";
        @NotNull final String newUsualMail = "tst@mail.ru";
        @NotNull final String newAdminLogin = "adm_usr";
        @NotNull final String newAdminPass = "adm_pass";
        @NotNull final String newAdminMail = "adm@mail.ru";

        @NotNull final UserRegistryRequest registryRequestUser = new UserRegistryRequest(newUsualLogin, newUsualPass, newUsualMail);
        @NotNull final UserRegistryRequest registryRequestAdmin = new UserRegistryRequest(newAdminLogin, newAdminPass, newAdminMail);

        @Nullable String newTokenUsual = AUTH_ENDPOINT.registry(registryRequestUser).getToken();
        AUTH_ENDPOINT.logout(new UserLogoutRequest(newTokenUsual));
        @Nullable String newTokenAdmin = AUTH_ENDPOINT.registry(registryRequestAdmin).getToken();
        AUTH_ENDPOINT.logout(new UserLogoutRequest(newTokenAdmin));

        @NotNull final UserRemoveRequest removeRequest1 = new UserRemoveRequest(getAdminToken(), newUsualLogin);
        @NotNull final UserRemoveRequest removeRequest2 = new UserRemoveRequest(getAdminToken(), newAdminLogin);
        Assert.assertNotNull(USER_ENDPOINT.removeUser(removeRequest1));
        Assert.assertNotNull(USER_ENDPOINT.removeUser(removeRequest2));
    }

    @Test
    public void testLogout() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getUsualToken());
        Assert.assertNotNull(AUTH_ENDPOINT.logout(request));
        @NotNull final String newPass = "tst_pass";
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(getUsualToken(), newPass);
        Assert.assertThrows(Exception.class, () -> USER_ENDPOINT.changePassword(changePasswordRequest));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(TEST_SOAP_LOGIN, TEST_SOAP_PASS);
        @Nullable final String token = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(token);
        TOKEN_SERVICE_USUAL.setToken(token);
    }

    @Test
    public void testUpdateViewProfile() {
        @NotNull final String firstName = "Иван";
        @NotNull final String middleName = "Иванович";
        @NotNull final String lastName = "Иванов";

        @NotNull final UserUpdateProfileRequest userUpdateProfileRequest = new UserUpdateProfileRequest(
                getUsualToken(), firstName, middleName, lastName
        );
        Assert.assertNotNull(USER_ENDPOINT.updateProfile(userUpdateProfileRequest));

        @NotNull final UserGetProfileRequest userGetProfileRequest = new UserGetProfileRequest(getUsualToken());
        @Nullable final User profile = USER_ENDPOINT.getProfile(userGetProfileRequest).getUser();
        Assert.assertNotNull(profile);
        Assert.assertEquals(firstName, profile.getFirstName());
        Assert.assertEquals(middleName, profile.getMiddleName());
        Assert.assertEquals(lastName, profile.getLastName());
    }

}