package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.malyugin.tm.dto.request.task.*;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.SoapCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

import static ru.t1.malyugin.tm.TestData.*;

@Category(SoapCategory.class)
public final class TaskEndpointTest {

    @BeforeClass
    public static void setToken() {
        @NotNull final String login = PROPERTY_SERVICE.getTestSoapLogin();
        @NotNull final String pass = PROPERTY_SERVICE.getTestSoapPass();
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(login, pass);
        @Nullable final String userToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(userToken);
        TOKEN_SERVICE_USUAL.setToken(userToken);
    }

    @AfterClass
    public static void clearToken() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(TOKEN_SERVICE_USUAL.getToken());
        AUTH_ENDPOINT.logout(request);
    }

    @NotNull
    private String getUserToken() {
        return TOKEN_SERVICE_USUAL.getToken();
    }

    @Before
    public void initTest() {
        final int numberOfTask = 3;
        for (int i = 1; i <= numberOfTask; i++) {
            @NotNull final TaskCreateRequest request = new TaskCreateRequest(
                    getUserToken(),
                    "NAME " + i,
                    "D " + i
            );
            @Nullable final Task task = TASK_ENDPOINT.creteTask(request).getTask();
            Assert.assertNotNull(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void after() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(getUserToken());
        Assert.assertNotNull(TASK_ENDPOINT.clearTask(request));
        TASK_LIST.clear();
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final Status status = Status.COMPLETED;
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(
                    getUserToken(),
                    task.getId(),
                    status
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.changeTaskStatusById(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(status, actualTask.getStatus());
        }
    }

    @Test
    public void testCompleteById() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(
                    getUserToken(),
                    task.getId()
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.completeTaskById(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
        }
    }

    @Test
    public void testStartById() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(
                    getUserToken(), task.getId()
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.startTaskById(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(Status.IN_PROGRESS, actualTask.getStatus());
        }
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(
                    getUserToken(), task.getId()
            );
            TASK_ENDPOINT.removeTaskById(request);
            @NotNull final TaskShowByIdRequest requestTask = new TaskShowByIdRequest(
                    getUserToken(), task.getId());
            Assert.assertNull(TASK_ENDPOINT.showTaskById(requestTask).getTask());
        }
    }

    @Test
    public void testShowById() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(
                    getUserToken(), task.getId()
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(task.getId(), actualTask.getId());
        }
    }

    @Test
    public void testShowList() {
        @NotNull final TaskShowListRequest request = new TaskShowListRequest(getUserToken(), null);
        @Nullable List<Task> actualList = TASK_ENDPOINT.showTaskList(request).getTaskList();
        Assert.assertNotNull(actualList);
        Assert.assertEquals(TASK_LIST.size(), actualList.size());
    }

    @Test
    public void testShowListByProjectId() {
        @NotNull final String projectName = "P1";
        @NotNull final String projectDescription = "D";

        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                getUserToken(), projectName, projectDescription
        );
        @Nullable final Project project = PROJECT_ENDPOINT.creteProject(requestCreateProject).getProject();
        Assert.assertNotNull(project);
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(
                    getUserToken(), task.getId(), project.getId()
            );
            Assert.assertNotNull(TASK_ENDPOINT.bindTaskToProject(requestBind));
        }
        @NotNull final TaskShowListByProjectIdRequest request = new TaskShowListByProjectIdRequest(
                getUserToken(), project.getId()
        );
        @Nullable final List<Task> actualList = TASK_ENDPOINT.showTaskListByProject(request).getTaskList();
        Assert.assertNotNull(actualList);
        Assert.assertEquals(TASK_LIST.size(), actualList.size());
    }

    @Test
    public void testBindTask() {
        @NotNull final String projectName = "P1";
        @NotNull final String projectDescription = "D";
        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                getUserToken(), projectName, projectDescription
        );
        @Nullable final Project project = PROJECT_ENDPOINT.creteProject(requestCreateProject).getProject();
        Assert.assertNotNull(project);
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                    getUserToken(), task.getId(), project.getId()
            );
            Assert.assertNotNull(TASK_ENDPOINT.bindTaskToProject(request));

            @NotNull final TaskShowByIdRequest requestActualTask = new TaskShowByIdRequest(getUserToken(), task.getId());
            @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(requestActualTask).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertNotNull(actualTask.getProjectId());
            @NotNull final String actualTaskProjectId = actualTask.getProjectId();
            Assert.assertEquals(project.getId(), actualTaskProjectId);
        }
    }

    @Test
    public void testUnbindTask() {
        @NotNull final String projectName = "P1";
        @NotNull final String projectDescription = "D";
        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                getUserToken(), projectName, projectDescription
        );
        @Nullable final Project project = PROJECT_ENDPOINT.creteProject(requestCreateProject).getProject();
        Assert.assertNotNull(project);
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                    getUserToken(), task.getId(), project.getId()
            );
            Assert.assertNotNull(TASK_ENDPOINT.bindTaskToProject(request));

            @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(
                    getUserToken(), task.getId(), project.getId()
            );
            Assert.assertNotNull(TASK_ENDPOINT.unbindTaskFromProject(requestUnbind));
            @NotNull final TaskShowByIdRequest requestActualTask = new TaskShowByIdRequest(getUserToken(), task.getId());
            @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(requestActualTask).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertNull(actualTask.getProjectId());
        }
    }

    @Test
    public void testClear() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(getUserToken());
        Assert.assertNotNull(TASK_ENDPOINT.clearTask(request));
        @NotNull final TaskShowListRequest requestList = new TaskShowListRequest(getUserToken(), null);
        Assert.assertNull(TASK_ENDPOINT.showTaskList(requestList).getTaskList());
    }

    @Test
    public void testCreate() {
        @NotNull final String name = "TEST_NAME";
        @NotNull final String description = "TEST_DESC";
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(
                getUserToken(), name, description
        );
        @Nullable final Task task = TASK_ENDPOINT.creteTask(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String name = "NEW_NAME";
        @NotNull final String description = "NEW_DESCRIPTION";
        @NotNull final Task task = TASK_LIST.get(0);
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(
                getUserToken(), task.getId(), name, description);
        Assert.assertNotNull(TASK_ENDPOINT.updateTaskById(request));

        @NotNull final TaskShowByIdRequest requestTask = new TaskShowByIdRequest(
                getUserToken(), task.getId());
        @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(requestTask).getTask();
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

}