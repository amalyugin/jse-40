package ru.t1.malyugin.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserRegistryResponse extends AbstractResponse {

    @Nullable
    private String token;

    public UserRegistryResponse(@Nullable final String token) {
        this.token = token;
    }

}