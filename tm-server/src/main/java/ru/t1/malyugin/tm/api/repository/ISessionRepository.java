package ru.t1.malyugin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert(
            "INSERT INTO tm.tm_session (row_id, user_id, created, role) " +
                    "VALUES (#{id}, #{userId}, #{created}, #{role})"
    )
    void add(@NotNull Session session);

    @Insert(
            "INSERT INTO tm.tm_session (row_id, user_id, created, role) " +
                    "VALUES (#{s.id}, #{userId}, #{s.created}, #{s.role})"
    )
    void addForUser(@NotNull @Param("userId") String userId, @NotNull @Param("s") Session session);

    @Nullable
    @Select("SELECT * FROM tm.tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Session> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_session WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Session findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm.tm_session LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Session findOneByIndex(@NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT COUNT(*) FROM tm.tm_session")
    Integer getSize();

    @Nullable
    @Select("SELECT COUNT(*) FROM tm.tm_session WHERE user_id = #{userId}")
    Integer getSizeForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm.tm_session")
    void clear();

    @Delete("DELETE FROM tm.tm_session WHERE user_id = #{userId}")
    void clearForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm.tm_session WHERE row_id = #{id}")
    void remove(@NotNull Session session);

}