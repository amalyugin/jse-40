package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.comparator.CreatedComparator;
import ru.t1.malyugin.tm.comparator.NameComparator;
import ru.t1.malyugin.tm.comparator.StatusComparator;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public ProjectService(
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }

    private void updateProject(@NotNull final Project project) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectRepository.update(project);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void addProject(@NotNull final Project project) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectRepository.add(project);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void removeProject(@NotNull final String userId, @NotNull final Project project) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable final List<Task> taskList = taskRepository.findAllByProjectId(userId, project.getId());
            if (taskList != null) taskList.forEach(t -> taskRepository.removeForUser(userId, t));
            projectRepository.removeForUser(userId, project);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession(false);
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(
            @Nullable final String userId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectRepository.clearForUser(userId);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new ProjectIdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        removeProject(userId, project);
    }

    @Override
    public void removeByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        removeProject(userId, project);
    }

    @Override
    public int getSize() {
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable final Integer size = projectRepository.getSize();
            if (size == null) return 0;
            return size;
        }
    }

    @Override
    public int getSize(
            @Nullable final String userId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable final Integer size = projectRepository.getSizeForUser(userId);
            if (size == null) return 0;
            return size;
        }
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable final Project project = projectRepository.findOneByIdForUser(userId, id);
            return project;
        }
    }

    @Nullable
    @Override
    public Project findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            return projectRepository.findOneByIndexForUser(userId, index);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable final List<Project> projects = projectRepository.findAll(userId);
            if (projects == null) return Collections.emptyList();
            return projects;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) {
        if (sort == null) return findAll(userId);
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable List<Project> projects = null;
            if (sort.getComparator() == CreatedComparator.INSTANCE)
                projects = projectRepository.findAllOrderByCreated(userId);
            if (sort.getComparator() == NameComparator.INSTANCE)
                projects = projectRepository.findAllOrderByName(userId);
            if (sort.getComparator() == StatusComparator.INSTANCE)
                projects = projectRepository.findAllOrderByStatus(userId);
            if (projects != null) return projects;
            return findAll(userId);
        }
    }

    @NotNull
    @Override
    @SuppressWarnings("rawtypes")
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) {
        if (comparator == null) return findAll(userId);
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable List<Project> projects = null;
            if (comparator == CreatedComparator.INSTANCE) projects = projectRepository.findAllOrderByCreated(userId);
            if (comparator == NameComparator.INSTANCE) projects = projectRepository.findAllOrderByName(userId);
            if (comparator == StatusComparator.INSTANCE) projects = projectRepository.findAllOrderByStatus(userId);
            if (projects != null) return projects;
            return findAll(userId);
        }
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setUserId(userId.trim());
        project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        addProject(project);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        updateProject(project);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId.trim(), index);
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        updateProject(project);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (status != null) project.setStatus(status);
        updateProject(project);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null) project.setStatus(status);
        updateProject(project);
        return project;
    }

}