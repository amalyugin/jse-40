package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.comparator.CreatedComparator;
import ru.t1.malyugin.tm.comparator.NameComparator;
import ru.t1.malyugin.tm.comparator.StatusComparator;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public TaskService(
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }

    private void updateTask(@NotNull final Task task) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void addTask(@NotNull final Task task) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void removeTask(@NotNull final String userId, @NotNull final Task task) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeForUser(userId, task);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession(false);
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.clearForUser(userId);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new TaskIdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        removeTask(userId, task);
    }

    @Override
    public void removeByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        removeTask(userId, task);
    }

    @Override
    public int getSize() {
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final Integer size = taskRepository.getSize();
            if (size == null) return 0;
            return size;
        }
    }

    @Override
    public int getSize(
            @Nullable final String userId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final Integer size = taskRepository.getSizeForUser(userId);
            if (size == null) return 0;
            return size;
        }
    }

    @Nullable
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new ProjectIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            return taskRepository.findOneByIdForUser(userId, id);
        }
    }

    @Nullable
    @Override
    public Task findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            return taskRepository.findOneByIndexForUser(userId, index);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable String userId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final List<Task> tasks = taskRepository.findAll(userId);
            if (tasks == null) return Collections.emptyList();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) {
        if (sort == null) return findAll(userId);
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable List<Task> tasks = null;
            if (sort.getComparator() == CreatedComparator.INSTANCE)
                tasks = taskRepository.findAllOrderByCreated(userId);
            if (sort.getComparator() == NameComparator.INSTANCE)
                tasks = taskRepository.findAllOrderByName(userId);
            if (sort.getComparator() == StatusComparator.INSTANCE)
                tasks = taskRepository.findAllOrderByStatus(userId);
            if (tasks != null) return tasks;
            return findAll(userId);
        }
    }

    @NotNull
    @Override
    @SuppressWarnings("rawtypes")
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) {
        if (comparator == null) return findAll(userId);
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable List<Task> tasks = null;
            if (comparator == CreatedComparator.INSTANCE) tasks = taskRepository.findAllOrderByCreated(userId);
            if (comparator == NameComparator.INSTANCE) tasks = taskRepository.findAllOrderByName(userId);
            if (comparator == StatusComparator.INSTANCE) tasks = taskRepository.findAllOrderByStatus(userId);
            if (tasks != null) return tasks;
            return findAll(userId);
        }
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @NotNull final Task task = new Task();
        task.setUserId(userId.trim());
        task.setName(name.trim());
        if (!StringUtils.isBlank(description)) task.setDescription(description.trim());
        addTask(task);
        return task;
    }

    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId.trim(), id.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        if (!StringUtils.isBlank(description)) task.setDescription(description.trim());
        updateTask(task);
        return task;
    }

    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @Nullable final Task task = findOneByIndex(userId.trim(), index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        if (!StringUtils.isBlank(description)) task.setDescription(description.trim());
        updateTask(task);
        return task;
    }

    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId.trim(), id.trim());
        if (task == null) throw new TaskNotFoundException();
        if (status != null) task.setStatus(status);
        updateTask(task);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId.trim(), index);
        if (task == null) throw new TaskNotFoundException();
        if (status != null) task.setStatus(status);
        updateTask(task);
        return task;
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.findOneByIdForUser(userId, projectId) == null) throw new ProjectNotFoundException();
        }
        @Nullable final Task task = findOneById(userId.trim(), taskId.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId.trim());
        updateTask(task);
        return task;
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.findOneByIdForUser(userId, projectId) == null) throw new ProjectNotFoundException();
        }
        @Nullable final Task task = findOneById(userId.trim(), taskId.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        updateTask(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) return Collections.emptyList();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
            if (taskList == null) return Collections.emptyList();
            return taskList;
        }
    }

}