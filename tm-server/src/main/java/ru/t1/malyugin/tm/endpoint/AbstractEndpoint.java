package ru.t1.malyugin.tm.endpoint;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.user.AccessDeniedException;
import ru.t1.malyugin.tm.exception.user.PermissionException;
import ru.t1.malyugin.tm.model.Session;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected Session checkPermission(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final Session session = getServiceLocator().getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new PermissionException();
        return session;
    }

    @NotNull
    protected Session checkPermission(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (StringUtils.isBlank(token)) throw new AccessDeniedException();
        return getServiceLocator().getAuthService().validateToken(token);
    }

}