package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void clear();

    void clear(
            @Nullable String userId
    );

    void removeById(
            @Nullable String userId,
            @Nullable String id
    );

    void removeByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    Task findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Task findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    List<Task> findAll(
            @Nullable String userId
    );

    @NotNull
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    );

    @NotNull
    @SuppressWarnings("rawtypes")
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    int getSize();

    @NotNull
    int getSize(
            @Nullable String userId
    );

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    Task changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    Task bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    @NotNull
    Task unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

}