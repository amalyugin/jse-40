package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void clear();

    void clear(
            @Nullable String userId
    );

    void removeById(
            @Nullable String userId,
            @Nullable String id
    );

    void removeByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    Project findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Project findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    List<Project> findAll(
            @Nullable String userId
    );

    @NotNull
    List<Project> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    );

    @NotNull
    @SuppressWarnings("rawtypes")
    List<Project> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    int getSize();

    int getSize(
            @Nullable String userId
    );

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    Project changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

}