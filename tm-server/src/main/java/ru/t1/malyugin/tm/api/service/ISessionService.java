package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.Session;

public interface ISessionService {

    void clear();

    void clear(@Nullable String userId);

    void remove(@Nullable Session session);

    @Nullable
    Session findOneById(@Nullable String id);

    void add(@Nullable Session session);

}