package ru.t1.malyugin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert(
            "INSERT INTO tm.tm_task (row_id, user_id, project_id, created, name, description, status) " +
                    "VALUES (#{id}, #{userId}, #{projectId}, #{created}, #{name}, #{description}, #{status})"
    )
    void add(@NotNull Task task);

    @Insert(
            "INSERT INTO tm.tm_task (row_id, user_id, project_id, created, name, description, status) " +
                    "VALUES (#{t.id}, #{userId}, #{t.projectId}, #{t.created}, #{t.name}, #{t.description}, #{t.status})"
    )
    void addForUser(@NotNull @Param("userId") String userId, @NotNull @Param("t") Task task);

    @Delete("DELETE FROM tm.tm_task")
    void clear();

    @Delete("DELETE FROM tm.tm_task WHERE user_id = #{userId}")
    void clearForUser(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_task WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm.tm_task LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneByIndex(@NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM tm.tm_task WHERE row_id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm.tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneByIndexForUser(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT COUNT(*) FROM tm.tm_task")
    Integer getSize();

    @Nullable
    @Select("SELECT COUNT(*) FROM tm.tm_task WHERE user_id = #{userId}")
    Integer getSizeForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm.tm_task WHERE row_id = #{id}")
    void remove(@NotNull Task task);

    @Delete("DELETE FROM tm.tm_task WHERE row_id = #{t.id} AND user_id = #{userId}")
    void removeForUser(@NotNull @Param("userId") String userId, @NotNull @Param("t") Task task);

    @Nullable
    @Select("SELECT * FROM tm.tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Update("UPDATE tm.tm_task " +
            "SET name = #{name}, description = #{description}, status = #{status}, project_id = #{projectId} " +
            "WHERE row_id = #{id}"
    )
    void update(@NotNull Task task);

}