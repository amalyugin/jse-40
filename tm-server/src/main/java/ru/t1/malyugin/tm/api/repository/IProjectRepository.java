package ru.t1.malyugin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert(
            "INSERT INTO tm.tm_project (row_id, user_id, created, name, description, status) " +
                    "VALUES (#{id}, #{userId}, #{created}, #{name}, #{description}, #{status})"
    )
    void add(@NotNull Project project);

    @Insert(
            "INSERT INTO tm.tm_project (row_id, user_id, created, name, description, status) " +
                    "VALUES (#{p.id}, #{userId}, #{p.created}, #{p.name}, #{p.description}, #{p.status})"
    )
    void addForUser(@NotNull @Param("userId") String userId, @NotNull @Param("p") Project project);

    @Delete("DELETE FROM tm.tm_project")
    void clear();

    @Delete("DELETE FROM tm.tm_project WHERE user_id = #{userId}")
    void clearForUser(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm.tm_project WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Project findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm.tm_project LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Project findOneByIndex(@NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM tm.tm_project WHERE row_id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Project findOneByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Project findOneByIndexForUser(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT COUNT(*) FROM tm.tm_project")
    Integer getSize();

    @Nullable
    @Select("SELECT COUNT(*) FROM tm.tm_project WHERE user_id = #{userId}")
    Integer getSizeForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm.tm_project WHERE row_id = #{id}")
    void remove(@NotNull Project project);

    @Delete("DELETE FROM tm.tm_project WHERE row_id = #{p.id} AND user_id = #{userId}")
    void removeForUser(@NotNull @Param("userId") String userId, @NotNull @Param("p") Project project);

    @Update(
            "UPDATE tm.tm_project " +
                    "SET name = #{name}, description = #{description}, status = #{status} WHERE row_id = #{id}"
    )
    void update(@NotNull Project project);

}