package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Session;
import ru.t1.malyugin.tm.model.User;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class SessionRepositoryTest {
    @NotNull
    private static User user1 = new User();

    @NotNull
    private static User user2 = new User();

    @BeforeClass
    public static void initUser() {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() {
        USER_SERVICE.removeById(user1.getId());
        USER_SERVICE.removeById(user2.getId());
    }

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_SESSION; i++) {
            @NotNull final Session session = new Session();
            if (i <= NUMBER_OF_SESSION / 2) session.setUserId(user1.getId());
            else session.setUserId(user2.getId());
            SESSION_REPOSITORY.add(session);
            SESSION_LIST.add(session);
        }
    }

    @After
    public void clearData() {
        SESSION_REPOSITORY.clear();
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        TASK_LIST.clear();
        PROJECT_LIST.clear();
        SESSION_LIST.clear();
    }

    @Test
    public void testAddSession() {
        final int initialNumberOfEntries = SESSION_REPOSITORY.getSize();
        final int expectedNumberOfEntries = initialNumberOfEntries + 1;
        @NotNull final Session session = new Session();
        session.setRole(Role.USUAL);
        SESSION_LIST.add(session);
        SESSION_REPOSITORY.add(session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize().intValue());
    }

    @Test
    public void testAddSessionForUser() {
        final int initialNumberOfEntries = SESSION_REPOSITORY.getSize();
        final int expectedNumberOfEntries = initialNumberOfEntries + 1;
        @NotNull final Session session = new Session();
        SESSION_LIST.add(session);
        SESSION_REPOSITORY.addForUser(user1.getId(), session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize().intValue());
        @Nullable final Session actualSession = SESSION_REPOSITORY.findOneById(session.getId());
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getId(), actualSession.getId());
    }

    @Test
    public void testRemoveSession() {
        for (int i = 0; i < SESSION_LIST.size(); i++) {
            @NotNull final Session session = SESSION_LIST.get(i);
            SESSION_REPOSITORY.remove(session);
            Assert.assertNull(SESSION_REPOSITORY.findOneById(session.getId()));
            SESSION_LIST.remove(session);
        }
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(SESSION_LIST.size(), SESSION_REPOSITORY.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() {
        final int expectedSize = (int) SESSION_LIST.stream().filter(s -> s.getUserId().equals(user1.getId())).count();
        Assert.assertEquals(expectedSize, SESSION_REPOSITORY.getSizeForUser(user1.getId()).intValue());
    }

}