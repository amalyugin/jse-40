package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.User;

import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private static User user1 = new User();

    @NotNull
    private static User user2 = new User();

    @BeforeClass
    public static void initUser() {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() {
        USER_SERVICE.removeById(user1.getId());
        USER_SERVICE.removeById(user2.getId());
    }

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project N " + i);
            project.setDescription("Desc " + i);
            if (i <= NUMBER_OF_PROJECTS / 2) project.setUserId(user1.getId());
            else project.setUserId(user2.getId());
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        PROJECT_LIST.clear();
        TASK_LIST.clear();
    }

    @Test
    public void testClear() {
        final Integer expectedNumberOfEntries = 0;
        PROJECT_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testClearForUser() {
        final Integer expectedNumberOfEntries = 0;

        PROJECT_REPOSITORY.clearForUser(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_PROJECTS, PROJECT_REPOSITORY.getSize().intValue());

        PROJECT_REPOSITORY.clearForUser(user1.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSizeForUser(user1.getId()));
        Assert.assertNotEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSizeForUser(user2.getId()));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(PROJECT_LIST.size(), PROJECT_REPOSITORY.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() {
        final int userProjectListSize = (int) PROJECT_LIST.stream().filter(p -> user1.getId().equals(p.getUserId())).count();
        Assert.assertEquals(userProjectListSize, PROJECT_REPOSITORY.getSizeForUser(user1.getId()).intValue());
    }

    @Test
    public void testAddProject() {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS + 1;
        @NotNull final Project project = new Project("NEW", "NEW");
        PROJECT_LIST.add(project);
        PROJECT_REPOSITORY.add(project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize().intValue());
    }

    @Test
    public void testAddProjectForUser() {
        final int expectedNumberOfEntries = PROJECT_REPOSITORY.getSizeForUser(user1.getId()) + 1;
        @NotNull final Project project = new Project();
        PROJECT_REPOSITORY.addForUser(user1.getId(), project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSizeForUser(user1.getId()).intValue());
        Assert.assertNull(PROJECT_REPOSITORY.findOneByIdForUser(user2.getId(), project.getId()));
    }

    @Test
    public void testRemove() {
        final int initialNumberOfEntries = PROJECT_REPOSITORY.getSize();
        final int expectedNumberOfEntries = initialNumberOfEntries - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_REPOSITORY.remove(project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize().intValue());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(project.getId()));
    }

    @Test
    public void testRemoveForUser() {
        final int initialNumberOfEntries = PROJECT_REPOSITORY.getSizeForUser(user1.getId());
        int expectedNumberOfEntries = initialNumberOfEntries - 1;
        @NotNull final Project project1 = PROJECT_REPOSITORY.findAll(user1.getId()).get(0);
        PROJECT_REPOSITORY.removeForUser(user1.getId(), project1);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSizeForUser(user1.getId()).intValue());
        Assert.assertNull(PROJECT_REPOSITORY.findOneByIdForUser(user1.getId(), project1.getId()));

        expectedNumberOfEntries = PROJECT_REPOSITORY.getSizeForUser(user2.getId());
        @NotNull final Project project2 = PROJECT_REPOSITORY.findAll(user2.getId()).get(0);

        PROJECT_REPOSITORY.removeForUser(user1.getId(), project2);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSizeForUser(user2.getId()).intValue());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_REPOSITORY.findAll(user1.getId());
        @NotNull final List<Project> expectedProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(actualProjectList.size(), expectedProjectList.size());
        for (int i = 0; i < actualProjectList.size(); i++) {
            Assert.assertEquals(expectedProjectList.get(i).getId(), actualProjectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllWithComparatorForUser() {

    }

    @Test
    public void testFindOneById() {
        for (@NotNull final Project project : PROJECT_LIST) {
            Assert.assertEquals(project.getId(), PROJECT_REPOSITORY.findOneById(project.getId()).getId());
        }
        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(id));
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final List<Project> userProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .collect(Collectors.toList());

        for (@NotNull final Project project : userProjectList) {
            Assert.assertEquals(project.getId(), PROJECT_REPOSITORY.findOneByIdForUser(user1.getId(), project.getId()).getId());
            Assert.assertNull(PROJECT_REPOSITORY.findOneByIdForUser(user2.getId(), project.getId()));
        }

        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(PROJECT_REPOSITORY.findOneByIdForUser(user1.getId(), id));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_REPOSITORY.findAll(user1.getId());
        Assert.assertNotNull(actualProjectList);
        @NotNull final List<Project> expectedProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(actualProjectList.size(), expectedProjectList.size());
        for (int i = 0; i < expectedProjectList.size(); i++)
            Assert.assertEquals(expectedProjectList.get(i).getId(), PROJECT_REPOSITORY.findOneByIndexForUser(user1.getId(), i).getId());
    }

    @Test
    public void testUpdate() {
        for (@NotNull final Project project : PROJECT_LIST) {
            @NotNull final String newName = "NEW PROJECT";
            @NotNull final String newDescription = "NEW PROJECT DESC";
            @NotNull final Status newStatus = Status.COMPLETED;
            project.setName(newName);
            project.setDescription(newDescription);
            project.setStatus(newStatus);
            PROJECT_REPOSITORY.update(project);
            @Nullable final Project newProject = PROJECT_REPOSITORY.findOneById(project.getId());
            Assert.assertNotNull(newProject);
            Assert.assertEquals(newName, newProject.getName());
            Assert.assertEquals(newDescription, newProject.getDescription());
            Assert.assertEquals(newStatus, newProject.getStatus());
        }
    }

}